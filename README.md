# UtilisateurBundle

## Installation

#### Ajouter le repository dans le fichier composer.json

````
"repositories": [
     {
         "type": "vcs",
         "url": "git@bitbucket.org:arobanteam/utilisateurbundle.git"
     }
 ],
````

#### Faire un composer require pour charger la librairie

````
composer require arobanteam/utilisateurbundle
````

#### Créer ou mettre a jour le fichier security.yaml
path: /config/packages/security.yaml
````
parameters:
  # Nom des chemins a utiliser pour les redirections
  redirect.login: default
  redirect.logout: default
  redirect.enregistrer: default
  redirect.mdp.oublie.email.inconnu: default
  redirect.mdp.oublie.erreur.token: default
  redirect.mdp.oublie: default
  redirect.reset.mdp: default

security:
  encoders:
    Aroban\Bundle\UtilisateurBundle\Entity\Utilisateur:
      algorithm: auto

  # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
  providers:
    # used to reload user from session & other features (e.g. switch_user)
    app_user_provider:
      entity:
        class: Aroban\Bundle\UtilisateurBundle\Entity\Utilisateur
        property: email
  firewalls:
    dev:
      pattern: ^/(_(profiler|wdt)|css|images|js)/
      security: false
    main:
      anonymous: true
      guard:
        authenticators:
          - Aroban\Bundle\UtilisateurBundle\Security\LoginFormAuthenticator
      remember_me:
        secret: '%kernel.secret%'
        lifetime: 2592000
      logout:
        path: app_logout
        # where to redirect after logout
        target: default


      # activate different ways to authenticate
      # https://symfony.com/doc/current/security.html#firewalls-authentication

      # https://symfony.com/doc/current/security/impersonating_user.html
      # switch_user: true

  # Easy way to control access for large sections of your site
  # Note: Only the *first* access control that matches will be used
  access_control:
  # - { path: ^/admin, roles: ROLE_ADMIN }
  # - { path: ^/profile, roles: ROLE_USER }
````

#### Ajouter les routes
path: /config/routes.yaml
````
bundle_aroban_utilisateur:
  resource: "@UtilisateurBundle/Resources/config/routes.yaml"
````

#### Ajouter le repository pour l'entity Utilisateur
path: App\Repository\UtilisateurRepository.php
````
<?php

namespace App\Repository;


use Aroban\Bundle\UtilisateurBundle\Entity\Utilisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Utilisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Utilisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Utilisateur[]    findAll()
 * @method Utilisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Utilisateur::class);
    }
}
````

#### Gérer l'ajout des utilisateurs avec des fixtures (optionel)
path: App\DataFixtures\UtilisateurFixtures.php
````
<?php

namespace App\DataFixtures;

use Aroban\Bundle\UtilisateurBundle\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UtilisateurFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $utilisateur = new Utilisateur();
        $utilisateur->setEmail('admin@admin.com')
            ->setPassword($this->passwordEncoder->encodePassword(
                $utilisateur,
                'admin'
            ));

        $manager->persist($utilisateur);

        $manager->flush();
    }
}
````