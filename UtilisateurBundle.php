<?php

namespace Aroban\Bundle\UtilisateurBundle;

use Aroban\Bundle\UtilisateurBundle\DependencyInjection\UtilisateurExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class UtilisateurBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }

    public function getContainerExtension()
    {
        if(null === $this->extension) {
            $this->extension = new UtilisateurExtension();
        }

        return $this->extension;
    }
}